$(document).ready(function () {
    var elements = {
        navElements: $('a[href^="#"]'),
        scrollToElement: function () {
            elements.navElements.each(function () {

                $(this).on('click', function (e) {
                    e.preventDefault();
                    var id = $(this).attr('href');

                    $('html, body').animate({
                        scrollTop: $(id).offset().top
                    }, 2000);

                });//end On
            });//end each
        },
            scrollElement : $('div.scrollTop'),

            scrollToTop : function(){

                $(window).scroll(function(){
                    var scrollValue = $(this).scrollTop();
                    console.log(scrollValue);
                    if(scrollValue>100){
                        elements.scrollElement.show(500);
                    }else{
                        elements.scrollElement.hide(500);
                    }
                });
                elements.scrollElement.on('click', function(){
                    $('html, body').animate({
                        scrollTop: 0
                    }, 1000);
                });
            }
    }


    console.log(elements.navElements);
    elements.scrollToElement();
    elements.scrollToTop();
});


