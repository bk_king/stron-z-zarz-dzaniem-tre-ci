
    $(document).ready(function(){
        ajaxQuery();
    });

    function objToString (array) {
        var i, length=array.length, url='';
        for(i=0;i<length; i++){
            url+=array[i].name+'='+array[i].value+'&';
        }
        url = url.substr(0, url.length-1);
        return url;
    }

    function ajaxQuery(){
        $.ajax({
            type: 'POST',
            url : 'index.php?action=ajax',
            dataType : 'json',
            success: function (json) {
                setValuesFromJson(json);
            }

        });
    }
    function setValuesFromJson (json){
        var i, length = json.length;
        for(i=0;i<length;i++){
            if(json[i].typeCollection==='header-content-1'){
                $('#about_us_'+json[i].id+' p').html(json[i].content);
                $('#about_us_modal'+json[i].id+' div.modal-body').html(json[i].modal);
            }else if(json[i].typeCollection==='header-content-2'){
                $('#offer_modal_'+json[i].id+' div.modal-body').html(json[i].modal);
            }
        }
    }





