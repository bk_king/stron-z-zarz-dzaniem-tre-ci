(function(){
    var divRightSide = document.querySelector('div.myRightBox');
    var divLeftSide = document.querySelector('div.myLeftBox');

    
    
    window.addEventListener('resize', setHeight);
    window.addEventListener('load', setHeightToBody);
    window.addEventListener('load', setHeight);
    

   


    function setHeightToBody(){
        if(this.innerWidth>768){
            document.body.style.height = this.innerHeight+'px';
            document.body.style.display = 'flex';
        }
        
        
    }
    function setHeight(){
        if(this.innerWidth>900){
            var divLeftSideHeight = divLeftSide.offsetHeight;
            divRightSide.style.height = divLeftSideHeight+'px';
        }
    }
}());

