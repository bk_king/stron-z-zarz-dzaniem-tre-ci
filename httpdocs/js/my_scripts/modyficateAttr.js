(function () {
    var editIcon  = $('span.glyphicon-pencil');
    var addIcon = $('span.glyphicon-plus-sign');

    addIcon.each(function(){
        $(this).on('click', function(){
            var type = $(this).data('type');
            var category = $(this).data('category');
            var section = $(this).data('section');
            var modalId = $(this).data('target');

            $(modalId+' input[name=type]').val(type);
            $(modalId+' input[name=category]').val(category);
            $(modalId+' input[name=section]').val(section);

            console.log(type, category, section, modalId);


        });
    });

    editIcon.each(function(){
        $(this).on('click', function(){
           var id = $(this).data('id');
            var section = $(this).data('section');
            var modalId = $(this).data('target');
            var table = $(this).data('table');

            $(modalId+' input[name=id]').val(id);
            $(modalId+' input[name=section]').val(section);
            $(modalId+' input[name=table]').val(table);

            console.log($(id+' input[name=section]'));
            console.log($(id+' input[name=id]'));
            console.log(id, section, modalId);


        })
    })





}());
