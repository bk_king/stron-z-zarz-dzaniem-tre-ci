<?php

use Clients\CMS\Config\Config;
use Clients\CMS\Session\Session;
use Clients\CMS\Request\Request;
use Clients\CMS\Controller\Controller;

require_once(__DIR__ . '/../vendor/autoload.php');

$twig = new Twig_Environment(
    new Twig_Loader_Filesystem(Config::ABSOLUTE_TEMPLATE_DIR),
    array(
        'cache' => (false === Config::DEBUG) ? Config::ABSOLUTE_CACHE_TEMPLATE_DIR : false
    )
);

$app = new Controller(
    new Request(),
    new Session(),
    $twig
);

$app->run();



