<?php

namespace Clients\CMS\Session;

/**
 * Class Session
 * @package Sda\Millionaires\Session
 */
class Session
{

    const PREFIX = 'ClientsCMS_';
    

    public function __construct()
    {
        session_start();
    }

    public function put($key, $value)
    {
        $_SESSION[self::PREFIX . $key] = $value;
    }
    
    public function get($key, $default = null)
    {
        return  (true === array_key_exists(self::PREFIX . $key, $_SESSION)) ?
            $_SESSION[self::PREFIX . $key] : $default;
    }

    public function kill()
    {
        session_destroy();
    }
}