<?php


namespace Clients\CMS\Routing;


class Routing
{
    const MAIN_PAGE = 'index';
    const TECHNICAL = 'psychoTechnical';
    const ADMIN_PANEL = 'admin';

    /* Admin Panel */

    const ADMIN_MAIN = 'adminMain';
    const ADMIN_VERIFY = 'adminVerify';
    const ADMIN_LOGOUT = 'logout';
    const ADMIN_HEADER = 'header';
    const ADMIN_EDIT = 'edit';
    const ADMIN_REMOVE = 'remove';
    const ADMIN_ADD_CONTENT = 'addContent';
    const ADMIN_ABOUT_US = 'about_us';
    const ADMIN_CONTACT = 'contact';
    const ADMIN_FOOTER = 'footer';
    const ADMIN_OFFER = 'offer';
    const AJAX = 'ajax';
    const ADMIN_JUMBOTRON = 'jumbotron';
    const ADMIN_IMAGES = 'getImages';
    const ADMIN_FILES = 'getFiles';
    const ADMIN_ADD_FILE = 'addFile';
}