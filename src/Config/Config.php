<?php
/**
 * Created by PhpStorm.
 * User: barkr
 * Date: 04.08.2016
 * Time: 10:10
 */

namespace Clients\CMS\Config;


class Config
{
    /* PATHS */
    const DEBUG = true;
    const ABSOLUTE_TEMPLATE_DIR = __DIR__ . '/../View/';
    const ABSOLUTE_CACHE_TEMPLATE_DIR  = __DIR__ . '/../../cache';
    /* DB Config */
    const DB_NAME_AND_HOST = 'mysql:host=localhost;dbname=centrumoptima';
    const PASS = '';
    const USER = 'root';


    /* DB TABLES NAMES*/

    const TITLE_CONTENT = 'header_content';
    const ELEMENT_CONTENT = 'elements';

    /*UNITS*/
    const MB = 1048576;


}