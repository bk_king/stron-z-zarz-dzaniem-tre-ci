<?php
/**
 * Created by PhpStorm.
 * User: barkr
 * Date: 17.08.2016
 * Time: 18:25
 */

namespace Clients\CMS\Config;


class Communicates
{
    /* Alerts for Files */
    const FILES_PATH = 'img/';
    const UPLOAD_OK = 'Plik został przesłany poprawnie';
    const INCORRECT_SIZE = 'przesyłany plik jest za duży';
    const INCORRECT_TYPE = 'przesyłany plik posiada niepoprawne rozszerzenie';
    const NOT_ACTUAL_FILE = 'to nie jest plik, który chcesz przesłać!';
    const UPLOAD_FAIL = 'Nie udało sie przesłać pliku';


    /*Account Communicates*/
    const LOGGED_ERROR = 'Login lub hasło jest niepoprawne';
}