<?php


namespace Clients\CMS;


/**
 * Created by PhpStorm.
 * User: barkr
 * Date: 08.08.2016
 * Time: 11:30
 */
class TypeCollection
{
    private $collection = [];
    private $typeCollection;

    protected function setTypeCollection($type)
    {
        if (empty($this->typeCollection)) {
            $this->typeCollection = $type;
        }
    }

    public function addToCollection($item)
    {
        if (is_object($item) === false ||
            empty($item) === true ||
            ($item instanceof $this->typeCollection) === false
        ) {

            return false;
        }
        $this->collection[] = $item;
    }

    public function getCollection(){
        return $this->collection;
    }


}