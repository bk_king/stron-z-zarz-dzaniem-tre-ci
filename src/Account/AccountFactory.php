<?php
/**
 * Created by PhpStorm.
 * User: barkr
 * Date: 06.08.2016
 * Time: 16:26
 */

namespace Clients\CMS\Account;


class AccountFactory
{


    public static function buildObject(array $result)
    {
        if (array_key_exists('id', $result) &&
            array_key_exists('name', $result) &&
            array_key_exists('login', $result) &&
            array_key_exists('password', $result) &&
            array_key_exists('email', $result)
        ) {
            return new AccountManagement(
                $result['id'],
                $result['name'],
                $result['login'],
                $result['password'],
                $result['email']);
        }
        return false;

    }
}