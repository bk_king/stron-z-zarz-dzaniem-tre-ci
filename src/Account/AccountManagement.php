<?php
/**
 * Created by PhpStorm.
 * User: barkr
 * Date: 04.08.2016
 * Time: 11:45
 */

namespace Clients\CMS\Account;


use Clients\CMS\Db\DbManagement;


class AccountManagement
{

    private $login;
    private $password;
    private $id;
    private $name;
    private $email;


    public function __construct($id, $name, $login, $password, $email)
    {
        $this->id = $id;
        $this->name = $name;
        $this->login = $login;
        $this->password = $password;
        $this->email = $email;

    }

    public function verifyAccount($passFromPost)
    {
        if (password_verify($passFromPost, $this->password)) {
            return true;
        }
        return false;

    }


}