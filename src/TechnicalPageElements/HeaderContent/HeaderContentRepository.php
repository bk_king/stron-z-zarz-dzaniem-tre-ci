<?php
/**
 * Created by PhpStorm.
 * User: barkr
 * Date: 11.08.2016
 * Time: 21:47
 */

namespace Clients\CMS\TechnicalPageElements\HeaderContent;


use Clients\CMS\Config\Config;
use Clients\CMS\Db\DbManagement;

class HeaderContentRepository
{
    private $collection = [];

    public function addToCollection($data){
        array_push($this->collection, $data);
    }
    public function setCollection($collection){
        $this->collection = $collection;
    }
    public function getCollection(){
        return $this->collection;
    }
    public function getJsonFromCollection(){
        $collection = $this->getCollection();
        $valuesArray = [];

        foreach ($collection as $val){
            array_push($valuesArray,[
                'modal'=>$val->getModal(),
                'id'=>$val->getId(),
                'content'=>$val->getContent(),
                'typeCollection'=>$val->getTypeCategory(),
                'header'=>$val->getHeader()
            ] );
        }
        echo json_encode($valuesArray);
    }
    public function getElementById($id)
    {
        $element = DbManagement::selectData(Config::TITLE_CONTENT, [[
            'column' => 'id',
            'value' => $id,
            'logic_oper' => '=',
            'oper' => '']], ['*'], 'row');
        if ($element === false) {
            return false;
        }
        return HeaderContentFactory::buildHeaderContent($element);
    }

    public function getElementsByTypeCategory($typeCategory)
    {
        $collection = [];
        $elements = DbManagement::selectData(Config::TITLE_CONTENT, [
            ['column' => 'type_category', 'value' => $typeCategory, 'logic_oper' => '=', 'oper' => '']
        ]);
        if ($elements === false) {
            return false;
        }
        foreach ($elements as $element) {
            $collection[] = HeaderContentFactory::buildHeaderContent($element);
        }
        return $collection;
    }

    public function edit(HeaderContent $element, $headerValue, $contentValue, $modal)
    {
        return DbManagement::updateData(Config::TITLE_CONTENT, [
            ['column' => 'header', 'value' => $headerValue],
            ['column' => 'content', 'value' => $contentValue],
            ['column' => 'modal', 'value' => $modal]
        ], [
            ['column' => 'id', 'value' => $element->getId()]
        ]);
    }

    public function remove(HeaderContent $element)
    {
        return DbManagement::deleteFromDb(Config::TITLE_CONTENT, [['column' => 'id', 'value' => $element->getId(), 'oper' => '=']]);
    }

    public function addContent($section, $header, $content, $typeCategory, $modal)
    {

        $element = HeaderContentFactory::buildByParams($section, $header, $content, $typeCategory, $modal);
        return DbManagement::insertIntoDb(Config::TITLE_CONTENT, [
            'section' => $element->getSection(),
            'header' => $element->getHeader(),
            'type_category' => $element->getTypeCategory(),
            'content' => $element->getContent(),
            'modal' => $element->getModal()
        ]);
    }


}