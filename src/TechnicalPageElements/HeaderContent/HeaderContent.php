<?php
/**
 * Created by PhpStorm.
 * User: barkr
 * Date: 10.08.2016
 * Time: 12:06
 */

namespace Clients\CMS\TechnicalPageElements\HeaderContent;


class HeaderContent
{
    private $id;
    private $header;
    private $content;
    private $section;
    private $typeCategory;
    private $modal;


    public function __construct($id, $header, $content, $section, $typeCategory, $modal)
    {
        $this->id=$id;
        $this->header=$header;
        $this->content=$content;
        $this->section=$section;
        $this->typeCategory=$typeCategory;
        $this->modal=$modal;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getHeader()
    {
        return $this->header;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @return mixed
     */
    public function getSection()
    {
        return $this->section;
    }

    /**
     * @return mixed
     */
    public function getTypeCategory()
    {
        return $this->typeCategory;
    }
    public function getModal()
    {
        return $this->modal;
    }
}