<?php
/**
 * Created by PhpStorm.
 * User: barkr
 * Date: 10.08.2016
 * Time: 12:13
 */

namespace Clients\CMS\TechnicalPageElements\HeaderContent;


class BuilderHeaderContent
{
    private $id ='';
    private $header='';
    private $content='';
    private $section='';
    private $typeCategory='';
    private $modal='';


    public function build(){
        return new HeaderContent($this->id, $this->header, $this->content, $this->section, $this->typeCategory, $this->modal);
    }
    public function withId($id){
        $this->id=$id;
        return $this;
    }

    public function withHeader($header){
        $this->header=$header;
        return $this;
    }

    public function withContent($content){
        $this->content=$content;
        return $this;
    }

    public function withSection($section){
        $this->section=$section;
        return $this;
    }

    public function withTypeCategory($typeCategory){
        $this->typeCategory=$typeCategory;
        return $this;
    }
    public function withModal($modal){
        $this->modal=$modal;
        return $this;
    }
}