<?php
/**
 * Created by PhpStorm.
 * User: barkr
 * Date: 10.08.2016
 * Time: 12:12
 */

namespace Clients\CMS\TechnicalPageElements\HeaderContent;


class HeaderContentFactory
{
    public static function buildHeaderContent(array $data)
    {
        if (array_key_exists('id', $data) &&
            array_key_exists('section', $data) &&
            array_key_exists('header', $data) &&
            array_key_exists('type_category', $data) &&
            array_key_exists('content', $data) &&
            array_key_exists('modal', $data)
        ) {
            $builder = new BuilderHeaderContent();
            return $builder
                ->withSection($data['section'])
                ->withId($data['id'])
                ->withHeader($data['header'])
                ->withTypeCategory($data['type_category'])
                ->withContent($data['content'])
                ->withModal($data['modal'])
                ->build();
        }
        return false;
    }

    public static function buildByParams($section, $header, $content, $typeCategory, $modal)
    {

        $builder = new BuilderHeaderContent();
        return $builder
            ->withSection($section)
            ->withHeader($header)
            ->withTypeCategory($typeCategory)
            ->withContent($content)
            ->withModal($modal)
            ->build();
    }

}