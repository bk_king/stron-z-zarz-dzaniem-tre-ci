<?php
/**
 * Created by PhpStorm.
 * User: barkr
 * Date: 08.08.2016
 * Time: 23:48
 */

namespace Clients\CMS\TechnicalPageElements;


use Clients\CMS\Db\DbManagement;

class PageElementsRepository
{
    public function getElementById($id)
    {
        $element = DbManagement::selectData('elements', [[
            'column' => 'id',
            'value' => $id,
            'logic_oper' => '=',
            'oper' => '']], ['*'], 'row');
        if ($element === false) {
            return false;
        }
        return PageElementFactory::buildByArray($element);
    }

    public function getElementsByParams($section, $type, $typeCategory)
    {
        $collection = [];
        $elements = DbManagement::selectData('elements', [
            ['column' => 'section', 'value' => $section, 'logic_oper' => '=', 'oper' => 'AND'],
            ['column' => 'type', 'value' => $type, 'logic_oper' => '=', 'oper' => 'AND'],
            ['column' => 'type_category', 'value' => $typeCategory, 'logic_oper' => '=', 'oper' => '']
        ]);
        if ($elements === false) {
            return false;
        }
        foreach ($elements as $element) {
            $collection[] = PageElementFactory::buildByArray($element);
        }
        return $collection;
    }
    public function concatArray($data){
        $newArray = [];
        foreach ($data as $value){
            $newArray[] = $value[0];
        }
        return $newArray;
    }

    public function edit(PageElement $element, $value)
    {
        return DbManagement::updateData('elements', [['column' => 'value', 'value' => $value]], [['column' => 'id', 'value' => $element->getId()]]);
    }

    public function remove(PageElement $element)
    {
        return DbManagement::deleteFromDb('elements', [['column' => 'id', 'value' => $element->getId(), 'oper' => '=']]);
    }

    public function addContent($section, $type, $value, $typeCategory)
    {
        $element = PageElementFactory::buildByParams($section, $type, $value, $typeCategory);
        return DbManagement::insertIntoDb('elements', ['section' => $element->getSection(),
            'type' => $element->getType(),
            'type_category' => $element->getTypeCategory(),
            'value' => $value
        ]);
    }


}