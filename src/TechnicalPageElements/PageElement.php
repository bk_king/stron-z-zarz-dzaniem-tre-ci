<?php
/**
 * Created by PhpStorm.
 * User: barkr
 * Date: 08.08.2016
 * Time: 23:38
 */

namespace Clients\CMS\TechnicalPageElements;


class PageElement
{
    private $id;
    private $type;
    private $value;
    private $section;
    private $typeCategory;

    public function __construct($id, $type, $value, $section, $typeCategory)
    {
        $this->id = $id;
        $this->section = $section;
        $this->type = $type;
        $this->typeCategory = $typeCategory;
        $this->value = $value;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @return mixed
     */
    public function getSection()
    {
        return $this->section;
    }

    /**
     * @return mixed
     */
    public function getTypeCategory()
    {
        return $this->typeCategory;
    }

}