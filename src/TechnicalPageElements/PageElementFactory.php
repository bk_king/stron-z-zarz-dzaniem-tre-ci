<?php
/**
 * Created by PhpStorm.
 * User: barkr
 * Date: 08.08.2016
 * Time: 23:41
 */

namespace Clients\CMS\TechnicalPageElements;


class PageElementFactory
{
    public static function buildByArray(array $data)
    {
        if (array_key_exists('id', $data) &&
            array_key_exists('section', $data) &&
            array_key_exists('type', $data) &&
            array_key_exists('type_category', $data) &&
            array_key_exists('value', $data)
        ) {
            $builder = new BuilderElement();
            return $builder->withSection($data['section'])
                ->withId($data['id'])
                ->withType($data['type'])
                ->withTypeCategory($data['type_category'])
                ->withValue($data['value'])
                ->build();
        }
        return false;
    }

    public static function buildByParams($section, $type, $value, $typeCategory)
    {
        $builder = new BuilderElement();
        return $builder
            ->withSection($section)
            ->withType($type)
            ->withTypeCategory($typeCategory)
            ->withValue($value)
            ->build();
    }
}