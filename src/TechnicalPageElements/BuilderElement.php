<?php
/**
 * Created by PhpStorm.
 * User: barkr
 * Date: 09.08.2016
 * Time: 08:51
 */

namespace Clients\CMS\TechnicalPageElements;


class BuilderElement
{
    private $id = '';
    private $type = '';
    private $typeCategory = '';
    private $section = '';
    private $value= '';

    public function build()
    {
        return new PageElement($this->id, $this->type, $this->value, $this->section, $this->typeCategory);
    }

    public function withSection($section)
    {
        $this->section = $section;
        return $this;
    }

    public function withType($type){
        $this->type = $type;
        return $this;
    }

    public function withTypeCategory($category){
        $this->typeCategory = $category;
        return $this;
    }
    public function withValue($value){
        $this->value = $value;
        return $this;
    }
    public function withId($id){
        $this->id = $id;
        return $this;
    }


}