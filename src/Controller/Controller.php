<?php


namespace Clients\CMS\Controller;


use Clients\CMS\Account\AccountFactory;
use Clients\CMS\Account\AccountManagement;
use Clients\CMS\Config\Communicates;
use Clients\CMS\Config\Config;
use Clients\CMS\Db\DbException;
use Clients\CMS\Db\DbManagement;
use Clients\CMS\Request\Request;
use Clients\CMS\Routing\Routing;
use Clients\CMS\Section\SectionRepository;
use Clients\CMS\SerwerManagement\SerwerManagement;
use Clients\CMS\Session\Session;
use Clients\CMS\TechnicalPageElements\HeaderContent\HeaderContentRepository;
use Clients\CMS\TechnicalPageElements\PageElementsRepository;
use Clients\CMS\Validators\ImageValidator;
use Twig_Environment;
use Twig_Error_Loader;
use Twig_Error_Runtime;
use Twig_Error_Syntax;


class Controller
{
    private $request;
    private $session;
    private $templates;
    private $pageElements;
    private $files;

    /**
     * Controller constructor.
     * @param Request $request
     * @param Session $session
     * @param Twig_Environment $twig
     */
    public function __construct(
        Request $request,
        Session $session,
        Twig_Environment $twig
    )
    {

        $this->request = $request;
        $this->session = $session;
        $this->templates = $twig;
        $this->pageElements = new PageElementsRepository();
        $this->headerContent = new HeaderContentRepository();
        $this->files = new ImageValidator();

    }

    /**
     * @param string $template
     * @param array $params
     */
    private function renderTemplate($template, array $params)
    {
        try {

            echo $this->templates->render($template, $params);

        } catch (Twig_Error_Loader $e) {
            echo $e->getMessage();
        } catch (Twig_Error_Syntax $e) {
            echo $e->getMessage();
        } catch (Twig_Error_Runtime $e) {
            echo $e->getMessage();
        }
    }

    private function redirect($routing)
    {
        header('Location: ?action=' . $routing);
        exit();
    }
public function validateImage()
{
    $params=[];
    $file = $this->request->getParamsFromFiles('image', false);
    if ($file) {
        if ($this->files->validateImage($file, Config::MB)) {
            $id = $this->request->getParamsFormPost('id', false);
            if ($id !== false) {
                if ($this->request->getParamsFormPost('table') === 'header_content') {
                    $element = $this->headerContent->getElementById($id);
                    $this->headerContent->edit($element, $element->getHeader(), $this->files->getName(), $element->getModal());
                } elseif ($this->request->getParamsFormPost('table') === 'elements') {
                    $element = $this->pageElements->getElementById($id);
                    $this->pageElements->edit($element, $this->files->getName());
                }
            }
            $params['flag']= 'true';
        }else{
            $params['flag'] = 'false';
        }
        $params['imageCommunicate'] = $this->files->getCommunicate();
    }
    return $params;
}

    public function run()
    {
        $action = $this->request->getParamFormGet('action', Routing::MAIN_PAGE);
        switch ($action) {
            case Routing::MAIN_PAGE:
                $this->renderTemplate('index.html', []);
                break;
            case Routing::AJAX:
                $params = [
                    'offer' => $this->headerContent->getElementsByTypeCategory('header-content-2'),
                    'aboutUsModals' => $this->headerContent->getElementsByTypeCategory('header-content-1')
                ];
                $data = array_merge_recursive($params['aboutUsModals'], $params['offer']);
                $this->headerContent->setCollection($data);
                $this->headerContent->getJsonFromCollection();
                break;
            case Routing::TECHNICAL:
                $params = [
                    'navbar' => $this->pageElements->getElementsByParams('header', 'li', 'li-1'),
                    'brand' => $this->pageElements->getElementsByParams('header', 'image', 'img-1'),
                    'jumbotron' => $this->pageElements->getElementsByParams('jumbotron', 'image', 'img-1'),
                    'aboutUsSentence' => $this->pageElements->getElementsByParams('about_us', 'h2', 'h2-1'),
                    'aboutUsPhoto' => $this->pageElements->getElementsByParams('about_us', 'image', 'photo'),
                    'aboutUsBackground' => $this->pageElements->getElementsByParams('about_us', 'image', 'background'),
                    'aboutUsParagraphs' => $this->pageElements->getElementsByParams('about_us', 'p', 'p-1'),
                    'aboutUsModals' => $this->headerContent->getElementsByTypeCategory('header-content-1'),
                    'offer' => $this->headerContent->getElementsByTypeCategory('header-content-2'),
                    'offerSentence' => $this->pageElements->getElementsByParams('offer', 'h2', 'h2-1'),
                    'offerBackground' => $this->pageElements->getElementsByParams('offer', 'image', 'background'),
                    'contactSentence' => $this->pageElements->getElementsByParams('contact', 'p', 'p-1'),
                    'contactHeader' => $this->pageElements->getElementsByParams('contact', 'h2', 'h2-1'),
                    'contactEmail' => $this->pageElements->getElementsByParams('contact', 'p', 'mail'),
                    'contactAddress' => $this->pageElements->getElementsByParams('contact', 'p', 'address'),
                    'contactPhone' => $this->pageElements->getElementsByParams('contact', 'p', 'phone'),
                    'contactFacebook' => $this->pageElements->getElementsByParams('contact', 'p', 'facebook'),
                    'footerCopyright' => $this->pageElements->getElementsByParams('footer', 'p', 'p-1'),
                    'footerHeaderOffer' => $this->pageElements->getElementsByParams('footer', 'h2', 'offer'),
                    'footerHeaderContact' => $this->pageElements->getElementsByParams('footer', 'h2', 'contact')
                ];
                $this->renderTemplate('psychoTechnical.html', $params);
                break;
            case Routing::ADMIN_PANEL:
                if ($this->session->get('logged') === 'true') {
                    $adminPanel = $this->request->getParamFormGet('adminPanel', Routing::ADMIN_MAIN);
                    switch ($adminPanel) {
                        case  Routing::ADMIN_MAIN:
                            $repository = new SectionRepository();
                            $params['sections'] = $repository->getSections();
                            $this->renderTemplate('adminMain.html', $params);
                            break;
                        case  Routing::ADMIN_IMAGES:
                            $params['imageValidation'] = $this->validateImage();
                            $imageList = SerwerManagement::getFilesTree('img/');
                            if($imageList){
                                $params['imageList'] = $imageList;
                            }

                            $this->renderTemplate('adminImages.html', $params);
                            break;
                        case  Routing::ADMIN_FILES:

                            $this->renderTemplate('adminFiles.html', []);
                            break;
                        case Routing::ADMIN_HEADER:
                            $params['imageValidation'] = $this->validateImage();
                            $params['lis'] = $this->pageElements->getElementsByParams('header', 'li', 'li-1');
                            $params['images'] = $this->pageElements->getElementsByParams('header', 'image', 'img-1');
                            $this->renderTemplate('adminSectionHeader.html', $params);
                            break;


                        case Routing::ADMIN_ABOUT_US:
                            $params['imageValidation'] = $this->validateImage();
                            $params['element_h2'] = $this->pageElements->getElementsByParams('about_us', 'h2', 'h2-1');
                            $params['elementsP'] = $this->pageElements->getElementsByParams('about_us', 'p', 'p-1');
                            $params['headerContent'] = $this->headerContent->getElementsByTypeCategory('header-content-1');
                            $params['photo'] = $this->pageElements->getElementsByParams('about_us', 'image', 'photo');
                            $params['background'] = $this->pageElements->getElementsByParams('about_us', 'image', 'background');
                            $this->renderTemplate('adminSectionAboutUs.html', $params);
                            break;

                        case Routing::ADMIN_OFFER:
                            $params['imageValidation'] = $this->validateImage();
                            $params['offers'] = $this->headerContent->getElementsByTypeCategory('header-content-2');
                            $params['aboutUs'] = $this->pageElements->getElementsByParams('offer', 'h2', 'h2-1');
                            $params['background'] = $this->pageElements->getElementsByParams('offer', 'image', 'background');
                            $this->renderTemplate('adminSectionOffer.html', $params);

                            break;
                        case Routing::ADMIN_JUMBOTRON:
                            $params['imageValidation'] = $this->validateImage();
                            $params['jumbotron'] = $this->pageElements->getElementsByParams('jumbotron', 'image', 'img-1');
                            $this->renderTemplate('adminSectionJumbotron.html', $params);

                            break;


                        case
                        Routing::ADMIN_CONTACT:
                            $params['elements_p'] = $this->pageElements->getElementsByParams('contact', 'p', 'p-1');
                            $params['elements_h2'] = $this->pageElements->getElementsByParams('contact', 'h2', 'h2-1');
                            $elementsArray[] = $this->pageElements->getElementsByParams('contact', 'p', 'phone');
                            $elementsArray[] = $this->pageElements->getElementsByParams('contact', 'p', 'mail');
                            $elementsArray[] = $this->pageElements->getElementsByParams('contact', 'p', 'facebook');
                            $elementsArray[] = $this->pageElements->getElementsByParams('contact', 'p', 'address');
                            $params['contactData'] = $elementsArray;

                            $this->renderTemplate('adminSectionContact.html', $params);
                            break;
                        case Routing::ADMIN_FOOTER:
                            $params['elements_p'] = $this->pageElements->getElementsByParams('footer', 'p', 'p-1');
                            $dataArray[] = $this->pageElements->getElementsByParams('footer', 'h2', 'offer');
                            $dataArray[] = $this->pageElements->getElementsByParams('footer', 'h2', 'contact');

                            $params['elementHeaders'] = $this->pageElements->concatArray($dataArray);

                            $this->renderTemplate('adminSectionFooter.html', $params);
                            break;
                        case Routing::ADMIN_EDIT:

                            $table = $this->request->getParamFormGet('table', false);
                            $id = $this->request->getParamFormGet('id', false);
                            $section = $this->request->getParamFormGet('section', false);

                            if ($table === false || $id === false || $section === false) {
                                $this->redirect(Routing::ADMIN_MAIN);
                            }

                            switch ($table) {
                                case Config::ELEMENT_CONTENT:

                                    $value = $this->request->getParamFormGet('value', false);


                                    if ($value === false || $section === false) {
                                        $this->redirect(Routing::ADMIN_MAIN);
                                    }
                                    $element = $this->pageElements->getElementById($id);
                                    $query = $this->pageElements->edit($element, $value);
                                    echo $element->getValue();


                                    break;
                                case Config::TITLE_CONTENT:

                                    $content = $this->request->getParamFormGet('content', false);
                                    $header = $this->request->getParamFormGet('header', false);
                                    $modal = $this->request->getParamFormGet('modal', false);

                                    if ($content === false || $header === false || $modal === false) {
                                        $this->redirect(Routing::ADMIN_MAIN);
                                    }

                                    $element = $this->headerContent->getElementById($id);
                                    $query = $this->headerContent->edit($element, $header, $content, $modal);

                                    break;
                            }


                            if ($query) {
                                $this->redirect('admin&adminPanel=' . $section);
                            }

                            break;
                        case Routing::ADMIN_REMOVE:

                            $table = $this->request->getParamFormGet('table', false);
                            $id = $this->request->getParamFormGet('id', false);
                            $section = $this->request->getParamFormGet('section', false);

                            if ($table === false || $id === false || $section === false) {
                                $this->redirect(Routing::ADMIN_MAIN);
                            }
                            switch ($table) {
                                case Config::ELEMENT_CONTENT:

                                    $element = $this->pageElements->getElementById($id);
                                    $query = $this->pageElements->remove($element);
                                    break;
                                case Config::TITLE_CONTENT:
                                    $element = $this->headerContent->getElementById($id);
                                    $query = $this->headerContent->remove($element);
                                    break;

                            }

                            if ($query) {
                                $this->redirect('admin&adminPanel=' . $section);
                            }
                            break;
                        case Routing::ADMIN_ADD_CONTENT:

                            $table = $this->request->getParamFormGet('table', false);
                            $section = $this->request->getParamFormGet('section', false);
                            if ($table === false ||
                                $section === false
                            ) {
                                $this->redirect(Routing::ADMIN_MAIN);
                            }
                            switch ($table) {
                                case Config::ELEMENT_CONTENT:

                                    $value = $this->request->getParamFormGet('value', false);
                                    $type = $this->request->getParamFormGet('type', false);
                                    $category = $this->request->getParamFormGet('category', false);

                                    if ($value === false ||
                                        $type === false ||
                                        $category === false
                                    ) {
                                        $this->redirect(Routing::ADMIN_MAIN);
                                    }
                                    $query = $this->pageElements->addContent($section, $type, $value, $category);
                                    break;
                                case Config::TITLE_CONTENT:
                                    $header = $this->request->getParamFormGet('header', false);
                                    $content = $this->request->getParamFormGet('content', false);
                                    $category = $this->request->getParamFormGet('category', false);
                                    $modal = $this->request->getParamFormGet('modal', false);

                                    if ($header === false ||
                                        $content === false ||
                                        $category === false ||
                                        $modal === false
                                    ) {
                                        $this->redirect(Routing::ADMIN_MAIN);
                                    }

                                    $query = $this->headerContent->addContent($section, $header, $content, $category, $modal);
                                    break;

                            }
                            if ($query) {
                                $this->redirect('admin&adminPanel=' . $section);
                            }
                            break;
                        case Routing::ADMIN_LOGOUT:
                            $this->session->kill();
                            $this->redirect(Routing::MAIN_PAGE);
                        default :
                            echo "nie ma strony błąd 404";
                            break;
                    }
                    return;

                }
                $login = $this->request->getParamsFormPost('login', false);
                $password = $this->request->getParamsFormPost('password', false);
                $params = [];
                if ($login !== false && $password !== false) {
                    $result = DbManagement::selectData('users', [
                        ['column' => 'login', 'logic_oper' => '=', 'oper' => '', 'value' => $login]],
                        ['*'], 'row');
                    if ($result) {
                        $object = AccountFactory::buildObject($result);
                        if ($object->verifyAccount($password)) {
                            $this->session->put('logged', 'true');
                            $this->redirect('admin&adminPanel=' . Routing::ADMIN_MAIN);
                        }
                    }
                    $params = [
                        'login' => $login,
                        'logged_error' => Communicates::LOGGED_ERROR
                    ];


                }
                $this->renderTemplate('adminPanel.html', $params);


                break;


        }
    }

}