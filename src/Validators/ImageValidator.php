<?php
/**
 * Created by PhpStorm.
 * User: barkr
 * Date: 17.08.2016
 * Time: 17:20
 */

namespace Clients\CMS\Validators;


use Clients\CMS\Config\Communicates;


class ImageValidator
{
    private $name;
    private $communicate;


    public function validateImage($data, $size)
    {
        if(!$this->checkActualImage($data)) return false;
        if(!$this->checkSize($data, $size)) return false;
        if(!$this->checkType($data)) return false;
        return $this->sendImage($data);
    }

    private function checkActualImage($data)
    {
        if (!is_uploaded_file($data['tmp_name'])) {
            $this->setCommunicate(Communicates::NOT_ACTUAL_FILE);
            return false;
        }
        return true;
    }

    private function checkSize($data, $size)
    {
        if ($data['size'] > $size) {
            $this->setCommunicate(Communicates::INCORRECT_SIZE);
            return false;
        }
        return true;
    }

    private function checkType($data)
    {
        $imageFileType = pathinfo($data['name'], PATHINFO_EXTENSION);


        if (
            $imageFileType !== 'jpg' &&
            $imageFileType !== 'png' &&
            $imageFileType !== 'jpeg' &&
            $imageFileType !== 'gif'
        ) {
            $this->setCommunicate(Communicates::INCORRECT_TYPE);
            return false;
        }
        return true;
    }

    private function sendImage($data)
    {
        $name = 'img/' . $data['name'];
        if (move_uploaded_file($data['tmp_name'], $name)) {
            $this->setCommunicate(Communicates::UPLOAD_OK);
            $this->setName($name);
            return true;
        }else{
            $this->setCommunicate(Communicates::UPLOAD_FAIL);
            return false;
        }
    }

    private function setCommunicate($value)
    {
        $this->communicate = $value;
    }

    public function getCommunicate()
    {
        return $this->communicate;
    }

    private function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }
}