<?php
/**
 * Created by PhpStorm.
 * User: barkr
 * Date: 08.08.2016
 * Time: 11:15
 */

namespace Clients\CMS\Section;


use Clients\CMS\Db\DbManagement;

class SectionRepository
{

    public function getSections()
    {
        $result = DbManagement::selectData('sections');
        if ($result !== false) {
            $collection = new SectionCollection();
            foreach ($result as $item) {
                $collection->addToCollection(SectionFactory::buildSection($item));
            }
            return $collection->getCollection();
        }
        return false;
    }
}