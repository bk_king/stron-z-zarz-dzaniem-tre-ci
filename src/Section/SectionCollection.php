<?php
/**
 * Created by PhpStorm.
 * User: barkr
 * Date: 08.08.2016
 * Time: 11:29
 */

namespace Clients\CMS\Section;


use Clients\CMS\TypeCollection;

class SectionCollection extends TypeCollection
{
    public function __construct()
    {
        $this->setTypeCollection(Section::class);
    }
}