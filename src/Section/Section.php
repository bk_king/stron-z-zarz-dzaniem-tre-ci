<?php
/**
 * Created by PhpStorm.
 * User: barkr
 * Date: 08.08.2016
 * Time: 10:52
 */

namespace Clients\CMS\Section;



class Section
{
    private $id;
    private $name;
    private $link;

    public function __construct($id, $name, $link)
    {
        $this->id = $id;
        $this->name = $name;
        $this->link = $link;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getLink()
    {
        return $this->link;
    }


}