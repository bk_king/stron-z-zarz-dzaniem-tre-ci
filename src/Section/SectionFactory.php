<?php
/**
 * Created by PhpStorm.
 * User: barkr
 * Date: 08.08.2016
 * Time: 11:01
 */

namespace Clients\CMS\Section;


class SectionFactory
{
    public static function buildSection(array $data)
    {
        if (array_key_exists('id', $data) &&
            array_key_exists('name', $data) &&
            array_key_exists('link', $data)
        ){
            return new Section($data['id'], $data['name'], $data['link']);
        }
    }
}